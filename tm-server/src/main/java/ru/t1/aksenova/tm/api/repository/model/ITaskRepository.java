package ru.t1.aksenova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.enumerated.TaskSort;
import ru.t1.aksenova.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    List<Task> findAll();

    @NotNull
    List<Task> findAll(@NotNull String userId);

    @NotNull
    List<Task> findAll(@NotNull String userId, @NotNull TaskSort sort);

    @NotNull
    List<Task> findAll(@NotNull String userId, @NotNull Comparator<Task> comparator);

    @Nullable
    Task findOneById(@NotNull String id);

    @Nullable
    Task findOneById(@NotNull String id, @NotNull String userId);

    void removeById(@NotNull String id);

    void removeById(@NotNull String id, @NotNull String userId);

    void clear();

    void clear(@NotNull String userId);

    long getCount();

    long getCount(@NotNull String userId);

    boolean existById(@NotNull String id);

    boolean existById(@NotNull String id, @NotNull String userId);

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeTaskByProjectId(@NotNull String projectId);

}
