package ru.t1.aksenova.tm.repository.model;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.aksenova.tm.api.repository.model.IUserRepository;
import ru.t1.aksenova.tm.model.User;

import java.util.List;
import java.util.Optional;

@Repository
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@AllArgsConstructor
public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    @Override
    public List<User> findAll() {
        @NotNull final String jpql = "SELECT m FROM User m";
        return entityManager.createQuery(jpql, User.class).getResultList();
    }

    @Nullable
    @Override
    public User findOneById(@Nullable final String id) {
        if (id.isEmpty() || id == null) return null;
        return entityManager.find(User.class, id);
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return null;
        @NotNull final String sql = "SELECT m FROM User m WHERE m.login = :login";
        return entityManager.createQuery(sql, User.class)
                .setParameter("login", login)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) return null;
        @NotNull final String sql = "SELECT m FROM User m WHERE m.email = :email";
        return entityManager.createQuery(sql, User.class)
                .setParameter("email", email)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        Optional<User> model = Optional.ofNullable(findOneById(id));
        model.ifPresent(this::remove);
    }

    @Override
    public void clear() {
        @NotNull final String jpql = "DELETE FROM User";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public void removeOneByLogin(@Nullable final String login) {
        Optional<User> model = Optional.ofNullable(findByLogin(login));
        model.ifPresent(this::remove);
    }

    @Override
    public void removeOneByEmail(@Nullable final String email) {
        Optional<User> model = Optional.ofNullable(findByEmail(email));
        model.ifPresent(this::remove);
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

}
