package ru.t1.aksenova.tm.api.service;

import liquibase.Liquibase;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

public interface ILiquibaseService {
    @SneakyThrows
    void closeConnectionLiquibase();

    @SneakyThrows
    void getConnectionLiquibase();

    @SneakyThrows
    Liquibase liquibase(@NotNull String filename);
}
