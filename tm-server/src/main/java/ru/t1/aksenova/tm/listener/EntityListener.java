package ru.t1.aksenova.tm.listener;

import lombok.NoArgsConstructor;
import org.hibernate.event.spi.*;
import org.hibernate.persister.entity.EntityPersister;
import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.log.OperationEvent;
import ru.t1.aksenova.tm.log.OperationType;

import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;

@NoArgsConstructor
public final class EntityListener implements PostInsertEventListener, PostDeleteEventListener, PostUpdateEventListener {

    @NotNull
    private JmsLoggerProducer jmsLoggerProducer;

    public EntityListener(@NotNull final JmsLoggerProducer jmsLoggerProducer) {
        this.jmsLoggerProducer = jmsLoggerProducer;
    }

    @Override
    @PostPersist
    public void onPostInsert(@NotNull final PostInsertEvent event) {
        log(OperationType.INSERT, event.getEntity());
    }

    @Override
    @PostRemove
    public void onPostDelete(@NotNull final PostDeleteEvent event) {
        log(OperationType.DELETE, event.getEntity());
    }

    @Override
    @PostUpdate
    public void onPostUpdate(@NotNull final PostUpdateEvent event) {
        log(OperationType.UPDATE, event.getEntity());
    }

    @Override
    public boolean requiresPostCommitHanding(@NotNull final EntityPersister persister) {
        return false;
    }

    private void log(@NotNull final OperationType type, @NotNull final Object entity) {
        @NotNull final OperationEvent operationEvent = new OperationEvent(type, entity);
        jmsLoggerProducer.send(operationEvent);
    }

}
