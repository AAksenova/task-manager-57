package ru.t1.aksenova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.dto.model.SessionDTO;

import java.util.List;

public interface ISessionDTORepository extends IUserOwnedDTORepository<SessionDTO> {

    @NotNull
    List<SessionDTO> findAll();

    @NotNull
    List<SessionDTO> findAll(@NotNull String userId);

    @Nullable
    SessionDTO findOneById(@NotNull String id);

    @Nullable
    SessionDTO findOneById(@NotNull String id, @NotNull String userId);

    void removeById(@NotNull String id);

    void removeById(@NotNull String id, @NotNull String userId);

    void clear();

    void clear(@NotNull String userId);

    long getCount();

    long getCount(@NotNull String userId);

    boolean existById(@NotNull String id);

    boolean existById(@NotNull String id, @NotNull String userId);

}
