package ru.t1.aksenova.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.t1.aksenova.tm.api.service.*;
import ru.t1.aksenova.tm.api.service.dto.*;
import ru.t1.aksenova.tm.dto.model.ProjectDTO;
import ru.t1.aksenova.tm.dto.model.TaskDTO;
import ru.t1.aksenova.tm.dto.model.UserDTO;
import ru.t1.aksenova.tm.endpoint.AbstractEndpoint;
import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@Component
public final class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    @Autowired
    protected static ApplicationContext context;

    @Getter
    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Getter
    @NotNull
    @Autowired
    private IProjectDTOService projectService;

    @Getter
    @NotNull
    @Autowired
    private ITaskDTOService taskService;

    @Getter
    @NotNull
    @Autowired
    private IProjectTaskDTOService projectTaskService;

    @Getter
    @NotNull
    @Autowired
    private ISessionDTOService sessionService;

    @Getter
    @NotNull
    @Autowired
    private IUserDTOService userService;

    @Getter
    @NotNull
    @Autowired
    private IAuthService authService;

    @Getter
    @NotNull
    @Autowired
    private IAdminService adminService;

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private AbstractEndpoint[] endpoints;

    @SneakyThrows
    public void initEndpoints() {
        Arrays.stream(endpoints).forEach(this::registry);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    public void start() {
        initEndpoints();
        initDemoData();
        loggerService.initJmsLogger();
        loggerService.info("** WELCOME TO SERVER TASK-MANAGER **");
        initPID();
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
    }

    private void stop() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initDemoData() {
        try {
            if (!userService.isLoginExist("test")) {
                @NotNull final UserDTO test = userService.create("test", "test", "test@test.ru");
                projectService.add(test.getId(), new ProjectDTO("One test project", Status.IN_PROGRESS));
                projectService.add(test.getId(), new ProjectDTO("Two test project", Status.NOT_STARTED));
                projectService.add(test.getId(), new ProjectDTO("Three test project", Status.COMPLETED));
                taskService.add(test.getId(), new TaskDTO("Alfa task", Status.IN_PROGRESS));
                taskService.add(test.getId(), new TaskDTO("Beta task", Status.COMPLETED));
                taskService.add(test.getId(), new TaskDTO("Gamma task", Status.NOT_STARTED));
            }
            if (!userService.isLoginExist("user")) {
                @NotNull final UserDTO user = userService.create("user", "user", "user@user.ru");
                projectService.add(user.getId(), new ProjectDTO("Six test project", Status.NOT_STARTED));
                taskService.add(user.getId(), new TaskDTO("Teuta task", Status.IN_PROGRESS));
            }
            if (!userService.isLoginExist("admin")) {
                @NotNull final UserDTO admin = userService.create("admin", "admin", Role.ADMIN);
                projectService.add(admin.getId(), new ProjectDTO("Four test project", Status.NOT_STARTED));
                projectService.add(admin.getId(), new ProjectDTO("Five test project", Status.COMPLETED));
                taskService.add(admin.getId(), new TaskDTO("Delta task", Status.IN_PROGRESS));
                taskService.add(admin.getId(), new TaskDTO("Eta task", Status.NOT_STARTED));
            }
        } finally {

        }
    }

}
