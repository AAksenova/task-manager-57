package ru.t1.aksenova.tm.service;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.resource.ClassLoaderResourceAccessor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.aksenova.tm.api.service.ILiquibaseService;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

@Service
public class LiquibaseService implements ILiquibaseService {

    @NotNull
    private static final ClassLoaderResourceAccessor ACCESSOR = new ClassLoaderResourceAccessor();
    @NotNull
    private static Database DATABASE;

    @Override
    @SneakyThrows
    public void closeConnectionLiquibase() {
        DATABASE.close();
    }

    @Override
    @SneakyThrows
    public void getConnectionLiquibase() {
        final Properties properties = new Properties();
        final InputStream inputStream = ClassLoader.getSystemResourceAsStream("liquibase.properties");
        properties.load(inputStream);
        final Connection connection = DriverManager.getConnection(
                properties.getProperty("url"),
                properties.getProperty("username"),
                properties.getProperty("password")
        );
        final JdbcConnection jdbcConnection = new JdbcConnection(connection);
        DATABASE = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(jdbcConnection);
    }

    @Override
    @SneakyThrows
    public Liquibase liquibase(final @NotNull String filename) {
        getConnectionLiquibase();
        return new Liquibase(filename, ACCESSOR, DATABASE);
    }

}
