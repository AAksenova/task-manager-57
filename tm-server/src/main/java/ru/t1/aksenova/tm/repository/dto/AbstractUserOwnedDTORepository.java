package ru.t1.aksenova.tm.repository.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.aksenova.tm.api.repository.dto.IUserOwnedDTORepository;
import ru.t1.aksenova.tm.comparator.CreatedComparator;
import ru.t1.aksenova.tm.comparator.NameComparator;
import ru.t1.aksenova.tm.comparator.StatusComparator;
import ru.t1.aksenova.tm.dto.model.AbstractUserOwnedModelDTO;

import java.util.Comparator;

@Getter
@Repository
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@AllArgsConstructor
public abstract class AbstractUserOwnedDTORepository<M extends AbstractUserOwnedModelDTO>
        extends AbstractDTORepository<M> implements IUserOwnedDTORepository<M> {

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == StatusComparator.INSTANCE) return "status";
        else if (comparator == NameComparator.INSTANCE) return "name";
        else if (comparator == CreatedComparator.INSTANCE) return "created";
        else return "created";
    }

    @Nullable
    @Override
    public M add(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty() || userId == null) return null;
        model.setUserId(userId);
        entityManager.persist(model);
        return model;
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty() || userId == null) return;
        model.setUserId(userId);
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty() || userId == null) return;
        model.setUserId(userId);
        entityManager.remove(model);
    }

}
