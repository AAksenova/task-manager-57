package ru.t1.aksenova.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.aksenova.tm.api.endpoint.ITaskEndpoint;
import ru.t1.aksenova.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.aksenova.tm.api.service.dto.ITaskDTOService;
import ru.t1.aksenova.tm.dto.model.SessionDTO;
import ru.t1.aksenova.tm.dto.model.TaskDTO;
import ru.t1.aksenova.tm.dto.request.*;
import ru.t1.aksenova.tm.dto.response.*;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.enumerated.TaskSort;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Controller
@NoArgsConstructor
@AllArgsConstructor
@WebService(endpointInterface = "ru.t1.aksenova.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    @Autowired
    private ITaskDTOService taskService;

    @Nullable
    @Autowired
    private IProjectTaskDTOService projectTaskService;

    @NotNull
    @Override
    @WebMethod
    public TaskChangeStatusByIdResponse changeTaskStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskChangeStatusByIdRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = session.getUserId();
        @Nullable final Status status = Status.toStatus(request.getStatus());
        @Nullable final TaskDTO task = taskService.changeTaskStatusById(userId, id, status);
        return new TaskChangeStatusByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskClearResponse clearTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskClearRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        taskService.removeAll(userId);
        return new TaskClearResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCompleteByIdResponse completeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCompleteByIdRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final TaskDTO task = taskService.changeTaskStatusById(userId, id, Status.COMPLETED);
        return new TaskCompleteByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCreateResponse createTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCreateRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final TaskDTO task = taskService.create(userId, name, description);
        return new TaskCreateResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskListResponse listTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskListRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String sortType = request.getSort();
        @Nullable final String userId = session.getUserId();

        @Nullable TaskSort sort = TaskSort.toSort(sortType);
        if (sort == null) {
            sort = TaskSort.toSort("BY_CREATED");
        }
        @NotNull final List<TaskDTO> tasks = taskService.findAll(userId, sort.getComparator());
        return new TaskListResponse(tasks);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskListByProjectIdResponse listTaskByProjectId(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskListByProjectIdRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String userId = session.getUserId();
        @NotNull final List<TaskDTO> tasks = taskService.findAllByProjectId(userId, projectId);
        return new TaskListByProjectIdResponse(tasks);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskRemoveByIdResponse removeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskRemoveByIdRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        taskService.removeOneById(userId, id);
        return new TaskRemoveByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskShowByIdResponse showTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskShowByIdRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final TaskDTO task = taskService.findOneById(userId, id);
        return new TaskShowByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskStartByIdResponse startTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskStartByIdRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final TaskDTO task = taskService.changeTaskStatusById(userId, id, Status.IN_PROGRESS);
        return new TaskStartByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUpdateByIdResponse updateTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUpdateByIdRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final TaskDTO task = taskService.updateById(userId, id, name, description);
        return new TaskUpdateByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUnbindFromProjectResponse unbindTaskFromProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUnbindFromProjectRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String projectId = request.getProjectId();
        projectTaskService.unbindTaskToProject(userId, projectId, taskId);
        return new TaskUnbindFromProjectResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskBindToProjectResponse bindTaskToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskBindToProjectRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String projectId = request.getProjectId();
        projectTaskService.bindTaskToProject(userId, projectId, taskId);
        return new TaskBindToProjectResponse();
    }

}

