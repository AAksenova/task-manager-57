package ru.t1.aksenova.tm.configuration;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import ru.t1.aksenova.tm.api.service.IDatabaseProperty;
import ru.t1.aksenova.tm.dto.model.ProjectDTO;
import ru.t1.aksenova.tm.dto.model.SessionDTO;
import ru.t1.aksenova.tm.dto.model.TaskDTO;
import ru.t1.aksenova.tm.dto.model.UserDTO;
import ru.t1.aksenova.tm.model.Project;
import ru.t1.aksenova.tm.model.Session;
import ru.t1.aksenova.tm.model.Task;
import ru.t1.aksenova.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

import static org.hibernate.cfg.AvailableSettings.*;

@Configuration
@ComponentScan("ru.t1.aksenova.tm")
public class ServerConfiguration {

    @Bean
    @NotNull
    public EntityManagerFactory entityManagerFactory(@NotNull IDatabaseProperty databaseProperty) {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(DRIVER, databaseProperty.getDatabaseDriver());
        settings.put(URL, databaseProperty.getDatabaseConnection());
        settings.put(USER, databaseProperty.getDatabaseUser());
        settings.put(PASS, databaseProperty.getDatabasePassword());
        settings.put(DIALECT, databaseProperty.getDatabaseHibernateDialect());
        settings.put(HBM2DDL_AUTO, databaseProperty.getDatabaseHibernateHbm2ddl());
        settings.put(SHOW_SQL, databaseProperty.getDatabaseHibernateShowSql());
        settings.put(FORMAT_SQL, databaseProperty.getDatabaseHibernateFormatSql());

        settings.put(USE_SECOND_LEVEL_CACHE, databaseProperty.getDatabaseCacheUseSecondLevelCache());
        settings.put(USE_QUERY_CACHE, databaseProperty.getDatabaseCacheUseQueryCache());
        settings.put(USE_MINIMAL_PUTS, databaseProperty.getDatabaseCacheUseMinimalPuts());
        settings.put(CACHE_REGION_PREFIX, databaseProperty.getDatabaseCacheRegionPrefix());
        settings.put(CACHE_REGION_FACTORY, databaseProperty.getDatabaseCacheRegionFactoryClass());
        settings.put(CACHE_PROVIDER_CONFIG, databaseProperty.getDatabaseCacheProviderConfigFileResourcePath());

        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources source = new MetadataSources(registry);
        source.addAnnotatedClass(ProjectDTO.class);
        source.addAnnotatedClass(TaskDTO.class);
        source.addAnnotatedClass(UserDTO.class);
        source.addAnnotatedClass(SessionDTO.class);
        source.addAnnotatedClass(Project.class);
        source.addAnnotatedClass(Task.class);
        source.addAnnotatedClass(User.class);
        source.addAnnotatedClass(Session.class);
        @NotNull final Metadata metadata = source.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @Bean
    @NotNull
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public EntityManager entityManager(@NotNull final EntityManagerFactory entityManagerFactory) {
        return entityManagerFactory.createEntityManager();
    }

}
