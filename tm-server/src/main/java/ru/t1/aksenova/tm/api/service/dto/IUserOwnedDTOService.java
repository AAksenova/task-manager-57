package ru.t1.aksenova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.api.repository.dto.IUserOwnedDTORepository;
import ru.t1.aksenova.tm.dto.model.AbstractUserOwnedModelDTO;

public interface IUserOwnedDTOService<M extends AbstractUserOwnedModelDTO> extends IUserOwnedDTORepository<M> {

    @NotNull
    IUserOwnedDTORepository<M> getRepository();

    @Override
    M add(@NotNull String userId, M model);

    @Override
    void update(@NotNull String userId, M model);

    @Override
    void remove(@NotNull String userId, M model);

}
