package ru.t1.aksenova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.aksenova.tm.api.service.IPropertyService;
import ru.t1.aksenova.tm.dto.request.InitSchemeRequest;
import ru.t1.aksenova.tm.dto.response.InitSchemeResponse;
import ru.t1.aksenova.tm.event.ConsoleEvent;

@Component
public final class ApplicationInitSchemeListener extends AbstractSystemListener {

    @NotNull
    public static final String NAME = "scheme-init";

    @NotNull
    public static final String DESCRIPTION = "Initialize database scheme.";

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@applicationInitSchemeListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final IPropertyService service = getPropertyService();
        System.out.println("[INIT DATABASE SCHEME]");
        @NotNull final String token = "cuefn+Az4fVqckVBzc/OKcJmnPZa93HDkaqZFGQVDZXK4SEmBvsdcgXiIQQRLb7B1zhP6GX5e8BulR7I+q+CkcN5hvT+yoqBLgJ3iQ9xVVNda+oPfnqSuC1nKrFxZmpk3uaAb5vc/0VuuNsYaAuCnar5QxFMC9aOyVXyxZORgdO1mLaK+6Xhn7XThG0+V4Q5";
        @NotNull final InitSchemeRequest request = new InitSchemeRequest(token);
        @NotNull final InitSchemeResponse response = getAdminEndpoint().initDBScheme(request);
        System.out.println("RESULT: " + response.getResult());
    }

}
