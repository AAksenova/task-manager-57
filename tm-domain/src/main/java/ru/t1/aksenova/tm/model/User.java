package ru.t1.aksenova.tm.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.enumerated.Role;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_user")
public class User extends AbstractUserOwnedModel {

    private static final long serialVersionUID = 1;

    @Nullable
    @Column(length = 50, nullable = false)
    private String login;

    @Nullable
    @Column(length = 100, name = "password_hash", nullable = false)
    private String passwordHash;

    @Nullable
    @Column(length = 50)
    private String email;

    @Nullable
    @Column(length = 100, name = "first_name")
    private String firstName;

    @Nullable
    @Column(length = 100, name = "last_name")
    private String lastName;

    @Nullable
    @Column(length = 100, name = "middle_name")
    private String middleName;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(length = 50, nullable = false)
    private Role role = Role.USUAL;

    @Column
    private boolean locked = false;

    @NotNull
    @JsonIgnore
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    @JsonIgnore
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Project> projects = new ArrayList<>();

    @NotNull
    @JsonIgnore
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Session> sessions = new ArrayList<>();

}
